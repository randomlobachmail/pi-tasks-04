#define _CRT_SECURE_NO_WARNINGS
#define MAX 20
#include <stdio.h>
#include <time.h>
char str[MAX][256] = {'\0'};
int i = 0;
void createArr()
{
	for (i = 0; i < MAX; i++)
	{
		fgets(str[i], 256, stdin);
		str[i][strlen(str[i])-1]=0;
		if (str[i][0] == '\0')
			break;
	}
}
void randomStr()
{
	int random=0;
	i--;
	if (i==-1)
		puts("You haven't entered a string\n");
	else
	{	
		random = rand() % (i-0+1)+0;
		printf("Random = %d\n", random);
		puts("Random string:");
		puts(str[random]);
	}
}
int main()
{
	srand(time(0));
	puts("Enter your strings, please. You can enter 20 strings max.");
	createArr();
	randomStr();
	return 0;
}