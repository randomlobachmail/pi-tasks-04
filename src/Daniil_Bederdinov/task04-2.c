#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 5

int main()
{
    srand(time(0));
    char str[N][256];
    int i = 0, j = 0;
    int strcount;
    while (i < N)
    {
        fgets(str[i], 256, stdin);
        i++;
    }
    strcount = rand() % (i - 1);
    puts("");
    while (str[strcount][j] > 10)
    {
        printf("%c",str[strcount][j]);
        j++;
    }
    puts("");
    return 0;
}