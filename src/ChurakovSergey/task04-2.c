#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 15

int main()
{
	srand(time(0));
	int i, j;
	i = 1;
	char str[N][256];
	fgets(str[0], 256, stdin);
	while (str[i - 1][0] > 10 && i < N)
	{
		fgets(str[i], 256, stdin);
		i++;
	}
	int StrNum;
	StrNum = (rand() % (i-1));
	j = 0;
	while ((unsigned int)str[StrNum][j] > 10) 
	{
		putchar(str[StrNum][j]);
		j++;
	}
	return 0;
}