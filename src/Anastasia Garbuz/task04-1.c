#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	char str[30][256];
	int i, K = 0;
	printf("Enter the number of strings: ");
	scanf("%d", &K);
	if (K > 30 || K < 1)
	{
		printf("ERROR\n");
		return 1;
	}
	puts("Enter strings:");
	for (i = 0; i <= K; i++)
	{
		fgets(str[i], 256, stdin);
		if (i != 0 && str[i][0] == '\n')
			break;
	}
	printf("\n");
	for (--i; i >= 0; i--)
		printf("%s", str[i]);
	return 0;
}