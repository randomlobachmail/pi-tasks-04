#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 10
#define L 20

int enterStrings(char (*s)[L])
{
    int i=0;
    for(i=0;i<N;i++)
    {
        fgets(s[i],L,stdin);
        s[i][strlen(s[i])-1]='\0';
        if (strlen(s[i])==0)
            return i;
    }
    return i;
}
void printStrings(char(*s)[L],int kol)
{
    int i;
    for(i=0;i<kol;i++)
    {
        printf("%s\n",s[i]);
    }
}
void printRand(char(*s)[L],int kol)
{
    int index=rand()%kol;
    printf("%s\n",s[index]);
}

int main(void)
{
    srand(time(0));
    char strings[N][L];
    int kol;
    kol=enterStrings(strings[0]);
    printRand(strings[0],kol);
   return 0;
}
