#include <stdio.h>
#include <string.h>
#define N 10
#define L 20

int enterStrings(char (*s)[L])
{
    int i=0;
    for(i=0;i<N;i++)
    {
        fgets(s[i],L,stdin);
        s[i][strlen(s[i])-1]='\0';
        if (strlen(s[i])==0)
            return i;
    }
    return i;
}
void printStrings(char(*s)[L],int kol)
{
    int i;
    for(i=0;i<kol;i++)
    {
        printf("%s\n",s[i]);
    }
}
void printSorted(char(*s)[L], int kol)
{
    int i,j,minLen=L+1,minIndex=-1;
    for(i=0;i<kol;i++)
    {
        for (j=0;j<kol;j++)
        {
            if (strlen(s[j])<minLen && strlen(s[j])!=0)
            {
                minLen=strlen(s[j]);
                minIndex=j;
            }
        }
        printf("%s\n",s[minIndex]);
        s[minIndex][0]='\0';
        minLen=L+1;

    }
}

int main(void)
{
    char strings[N][L];
    int kol;
    kol=enterStrings(strings[0]);
    printSorted(strings[0],kol);

    return 0;
}
