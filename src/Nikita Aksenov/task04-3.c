#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include<stdio.h>
int result(char str[][256], int num)
{
	int buf[N] = {0};
	int i = 0, l = -1;
	while (i <= num)
	{
		for (int j = 0; j < 256; j++)
		{
			if (str[i][j] == '\0')
			{
				buf[i] = l;
				break;
			}
			l++;
		}
		l = -1;
		i++;
	}
	printf("\n");
	l = 1;
	for (int i = 0; i < 256; i++)
		for (int p = 1; p <= num; p++)
			if (buf[p] == i)
			{
				printf("%s", str[p]);
				l++;
			}
			return l;
}
int main() {
	int num, i;
	char line[N][N] = { 0 };
	printf("Enter the number of lines, please\n");
	scanf("%d", &num);
	printf("\nEnter lines, please:\n", num);
	for (i = 0; i <= num; i++)
		fgets(line[i], 256, stdin);
	printf("\n");	
	printf("Result:");
	result(line,num);
	return 0;
}