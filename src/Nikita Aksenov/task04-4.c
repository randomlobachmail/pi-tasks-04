#define _CRT_SECURE_NO_WARNINGS
#define N 5
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void shuffle(char(*arr)[256], int n)
{
    srand(time(NULL));
    int index;
    int buf[N];
    for (int i = 0; i < n; i++)
        buf[i] = i;
    for (int i = n; i > 0; i--)
    {
        index = rand() % i;
        printf("%s", arr[buf[index]]);
        arr[buf[index]][0] = 0;
        buf[index] = buf[i - 1];
    }
}
int main()
{
    char str[N][256];
    int i = 0;
	printf("Enter strings:\n");
    while (i < N)
    {
        fgets(str[i], 256, stdin);
        i++;
    }
	printf("Result:\n");
    shuffle(str,N);
    return 0;
}