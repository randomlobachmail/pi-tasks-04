#include <stdio.h>
#include <time.h>
#define N 10

typedef char String[256];

int shuffleArray(int *arr, int arrayLength)
{
    int i, temp, r;
    srand(time(0));
    for (i = 0; i < arrayLength; i++)
    {
        r = rand() % arrayLength;
        temp = arr[i];
        arr[i] = arr[r];
        arr[r] = temp;
    }
}

int main()
{
    int i = 0, j;
    String s[N];
    int sIndex[N]; // ������ �������� ������� �����
    for (i = 0; i < N; i++)
    sIndex[i] = i;

    i = 0;
    fgets(s[0], 256, stdin);
    while ((s[i++][0]!='\n') && i<N)
        fgets(s[i], 256, stdin);
    i--; // ��� ��� ��� ����� ��������� ���� ������ ���������, ���������� ���������
    if (s[i][0]=='\n')
        i--; //���� ���� ���������� ������ ������ ������, ��������� �������, ����� �� ������� �

    shuffleArray(sIndex, i + 1);

    for (j = 0; j <= i; j++)
    printf("%s", s[sIndex[j]]);
    return 0;
}
