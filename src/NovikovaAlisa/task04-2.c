#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include<stdio.h>
#include<time.h>
int main(){
	int num, i;
	char line[N][N] = { 0 };
	printf("Enter the number of lines, please\n");
	scanf("%d", &num);
	printf("\nEnter %d lines, please:\n", num);
	for (i = 0; i <= num; i++)
		fgets(line[i], 256, stdin);
	printf("\n Your string:\n");
	for (i = 1; i <= num; i++)
		printf(" %d = %s", i, line[i]);
	printf("\n Result: \n");
	srand(time(NULL));
	 i = rand() % num;
	 int flag = 0;
	 while ( flag != 1){
		 if (i == 0){
			 i = rand() % num;
			 flag = 0;
		 }
		 else {
			 printf(" %d=%s", i, line[i]);
			 flag = 1;
		 }
}
	printf("\n\n");
	return 0;
}