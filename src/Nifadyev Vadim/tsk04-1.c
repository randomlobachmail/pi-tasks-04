/*�������� ���������, ����������� �� ������������ ��������� �����.
������ �������� ���� �� ���������� ������������� ���������� (N) ���� �� ����� ������ ������.
������� ������ �� ����� � �������� �������.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define N 20
#define M 256

int stringCounter = 0; // ������� �����
char strings[N][M] = { 0 };

void inputStrings() // ���� �����
{
	int i, j;
	printf("Enter the strings (max %d strings)\n", N);
	for (j = 0; j < N; j++)
	{
		fgets(strings[j], M, stdin);
		for (i = 0; i < M; i++)
		{
			if ((strings[j][i] == '\0' && strlen(strings[j]) != 1) || strings[j][i] == EOF) // ����� �������� ������
			{
				stringCounter++;
				break;
			}
			if (strings[j][i] == EOF) // ����� �����
				break;
		}
		if ((strlen(strings[j]) == 1) || (strlen(strings[j]) == 0)) // ���������� �����
			break;
	}
}

void reversedStringsOutput(char(*strings)[M], int size)
{
	int i;
	printf("Strings in reversed order\n");
	for (i = stringCounter; i > 0; i--) // ����� ����� � �������� �������
		printf("%s", strings[i]);
}

int main()
{
	inputStrings();
	reversedStringsOutput(strings, N);
	return 0;
}
