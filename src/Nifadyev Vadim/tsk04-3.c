/*�������� ���������, ����������� �� ������������ ��������� �����.
������ �������� ���� �� ���������� ������������� ���������� (N) ���� �� ����� ������ ������.
������� ������ �� ����� � ������� ����������� �� ����.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define N 20
#define M 256

int stringCounter = 0; // ������� �����
int stringsIndexsAndLenghs[2][N] = { 0 }; // ��������� ������ �������� ����� � �� ����
char strings[N][M] = { 0 };

void inputStrings() // ���� �����
{
	int i, j;
	printf("Enter the strings (max %d strings)\n", N);
	for (j = 0; j < N; j++)
	{
		fgets(strings[j], M, stdin);
		for (i = 0; i < M; i++)
		{
			if ((strings[j][i] == '\0' && strlen(strings[j]) != 1) || strings[j][i] == EOF) // ����� �������� ������
			{
				stringsIndexsAndLenghs[0][j] = j; // ������� � ������ ������ �������
				stringsIndexsAndLenghs[1][j] = strlen(strings[j]); // ����� ����� �� ������
				stringCounter++;
				break;
			}
			if (strings[j][i] == EOF) // ����� �����
				break;
		}
		if ((strlen(strings[j]) == 1) || (strlen(strings[j]) == 0)) // ���������� �����
			break;
	}
}

void sortedStringsOutput(char(*strings)[M], int size) // ����� ����� �� ����������� �� ����
{
	int i, j, temp;
	for (i = 0; i < stringCounter; i++) 
		for (j = 0; j < stringCounter; j++)
			if (stringsIndexAndLengh[1][i] < stringsIndexAndLengh[1][j]) // ������ i-��� ������� ������� � ����������� �� �� ����
			{
				temp = stringsIndexAndLengh[0][i];
				stringsIndexAndLengh[0][i] = stringsIndexAndLengh[0][j];
				stringsIndexAndLengh[0][j] = temp;
				temp = stringsIndexAndLengh[1][i];
				stringsIndexAndLengh[1][i] = stringsIndexAndLengh[1][j];
				stringsIndexAndLengh[1][j] = temp;
			}
	printf("Strings sorted by their lengh (from min to max)\n");
	for (i = 0; i < stringCounter; i++)
		printf("%s", strings[stringsIndexAndLengh[0][i]]);
}


int main()
{
	inputStrings();
	sortedStringsOutput(strings, N);
	return 0;
}

