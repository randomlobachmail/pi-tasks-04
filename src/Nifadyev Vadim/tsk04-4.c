/*�������� ���������, ����������� �� ������������ ��������� �����.
������ �������� ���� �� ���������� ������������� ���������� (N) ���� �� ����� ������ ������.
������� ������ �� ����� � ��������� �������.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 20
#define M 256

int stringCounter = 0; // ������� �����
int stringsIndexsAndLenghs[2][N] = { 0 }; // ��������� ������ �������� ����� � �� ����
char strings[N][M] = { 0 };

void inputStrings()
{
	int i, j;
	printf("Enter the strings (max %d strings)\n", N);
	for (j = 0; j < N; j++)
	{
		fgets(strings[j], M, stdin);
		for (i = 0; i < M; i++)
		{
			if ((strings[j][i] == '\0' && strlen(strings[j]) != 1) || strings[j][i] == EOF) // ����� �������� ������
			{
				stringsIndexsAndLenghs[0][j] = j; // ������� � ������ ������ �������
				stringsIndexsAndLenghs[1][j] = strlen(strings[j]); // ����� ����� �� ������
				stringCounter++;
				break;
			}
			if (strings[j][i] == EOF) // ����� �����
				break;
		}
		if ((strlen(strings[j]) == 1) || (strlen(strings[j]) == 0)) // ���������� �����
			break;
	}
}



void randomPrintStrings(char(*strings)[M], int size) // ����� �� ����� ����� � ��������� �������
{
	int i, randomNumber;
	srand(time(NULL));
	for (i = stringCounter; i > 0; i--)
	{
		randomNumber = rand() % i;
		printf("%s", strings[stringsIndexsAndLenghs[0][randomNumber]]);
		stringsIndexsAndLenghs[0][randomNumber] = stringsIndexsAndLenghs[0][i - 1]; // �������� �������� ���������� �������� �������
	}
}

int main()
{
	inputStrings();
	randomPrintStrings(strings, N);
	return 0;
}

